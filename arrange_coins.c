#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/*
 * 	Link: https://leetcode.com/problems/arranging-coins/
 */

int arrangeCoins(int n)
{
	unsigned long long m = n;
    double x = sqrt(2*n);
    unsigned long long i = x;

	if ( 2*m == i*i + i)
		return i;

    return i - 1;

}


int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	printf("%d\n", arrangeCoins(n));
	return 0;
}
