#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/sign-of-the-product-of-an-array/
 */

int arraySign(int* nums, int numsSize)
{
	int countn = 0;
	for (int i = 0; i < numsSize; ++i)
	{
		if (nums[i] == 0)
			return 0;

		if (nums[i] < 0 )
			countn += 1;
	}

	if ( countn % 2 )
		return -1;
	else
		return 1;
	
}

int main(int argc, char *argv[])
{
	int n;
	scanf("%d",&n);

	int *a = (int*)malloc( sizeof(int) * n );
	for (int i = 0; i < n; ++i)
		scanf("%d",&a[i]);

	printf("%d\n", arraySign(a, n));

	free(a);
	return 0;
}
