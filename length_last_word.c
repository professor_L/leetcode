#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/length-of-last-word/
 */

int lengthOfLastWord(char * s)
{
    char *tok = strtok(s, " ");
    int len;
    do
    {
        len = strlen(tok);
        tok = strtok(NULL, " ");
    }while(tok);
    return len;
}

int main(int argc, char *argv[])
{
	int result = lengthOfLastWord(argv[1]);
	printf("%d\n",result);
	return 0;
}
