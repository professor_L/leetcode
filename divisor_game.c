/*
 *  Problem link:
 *  https://leetcode.com/problems/divisor-game/
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 *	Link: https://leetcode.com/problems/divisor-game/
 *
 */


bool divisorGame(int n){
   return n % 2 == 0;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	if ( divisorGame(n) )
		printf("TRUE");
	else
		printf("FALSE");
}
