#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/*
 * 	Link: https://leetcode.com/problems/sqrtx/
 */

int mySqrt(int n)
{
	double x = log10(n)/log10(2);
	printf("%lf\n%lf", x, sqrt(n) );
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	mySqrt(n);
	return 0;
}
