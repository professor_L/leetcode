#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * 	Link: https://leetcode.com/problems/add-strings/
 */

static inline void swap(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

char * addBinary(char * a, char * b)
{
	int sl, ll;
	char *ss, *ls;

	//find shorter and longer string
	sl = strlen(a);
	ll = strlen(b);

	ss = a;
	ls = b;

	if ( sl > ll )
	{
		swap(&sl, &ll);
		ss = b;
		ls = a;
	}

	//perform calculation
	char *c = (char*)calloc( ll + 2, 1 );
	
	int li = ll - 1;
	int si = sl - 1;
	int ci = ll;

	char tmp, residual = '0';

	while(si >= 0)
	{
		tmp = ss[si] + ls[li] + residual - 3*'0' ;
		c[ci] = ( tmp % 10 ) + '0';
		residual = (tmp / 10) + '0';
		si--;
		li--;
		ci--;
	}

	while ( li >= 0)
	{
		tmp = ls[li] + residual - 2*'0' ;
		c[ci] = ( tmp % 10 ) + '0';
		residual = (tmp / 10) + '0';
		li--;
		ci--;
	}

	if ( residual != '0' )
		c[0] = '1';
	else
	{
		memmove(c, c+1, ll);
		c[ll] = 0;
	}

	return c;
}

int main(int argc, char *argv[])
{
	char *c = addBinary(argv[1], argv[2]);
	printf("%s\n%s\n%s\n", argv[1], argv[2], c);
	free(c);
}
