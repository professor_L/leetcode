#include <stdio.h>
#include <stdlib.h>

/*
 * Link: https://leetcode.com/problems/add-two-numbers/
 */

struct ListNode {
	int val;
	struct ListNode *next;
};

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2)
{
	struct ListNode *ret = NULL, *tmp = NULL, *last = NULL, *ll = NULL, *sl = NULL;
	int residual = 0;
	int sum = 0;

	//calculate length of lists
	int len1 = 1, len2 = 1;
	tmp = l1;
	while( tmp->next != NULL )
	{
		len1++;
		tmp = tmp->next;
	}

	tmp = l2;
	while( tmp->next != NULL )
	{
		len2++;
		tmp = tmp->next;
	}

	//assign long list and short list
	if ( len1 > len2 )
	{
		ll = l1;
		sl = l2;
	}
	else
	{
		ll = l2;
		sl = l1;
	}

	do
	{
		sum = sl->val + ll->val + residual;
		residual = sum / 10;
		//sum = sum % 10;

		//create new node
		tmp = (struct ListNode*)malloc( sizeof(*tmp) );
		tmp->val = sum % 10;
		tmp->next = NULL;
        
        //add new node to THE END of list
        if ( ret == NULL )
        {
		    ret = tmp;
            last = ret;
        }
        else
        {
            last->next = tmp;
            last = tmp;
        }

		//update sl and ll
		sl = sl->next;
		ll = ll->next;
    }while(sl);

	while(ll)
	{
		sum = ll->val + residual;
		residual = sum / 10;

		//create new node
		tmp = (struct ListNode*)malloc( sizeof(*tmp) );
		tmp->val = sum % 10;
		tmp->next = NULL;

        //add new node to THE END of list
        if ( ret == NULL )
        {
		    ret = tmp;
            last = ret;
        }
        else
        {
            last->next = tmp;
            last = tmp;
        }

		//update ll
		ll = ll->next;
	}
    
    if (residual != 0)
    {
        //create new node
		tmp = (struct ListNode*)malloc( sizeof(*tmp) );
		tmp->val = residual;
		tmp->next = NULL;
                
        last->next = tmp;
        last = tmp;
        
    }
    return ret;
}

struct ListNode* create_list(int n)
{
	struct ListNode *ret = NULL, *tmp = NULL;
	int val;
	while(n)
	{
		scanf("%d", &val);
		tmp = (struct ListNode*)malloc( sizeof(*tmp) );
		tmp->val = val;
		tmp->next = NULL;

		if (ret == NULL)
			ret = tmp;
		else
		{
			tmp->next = ret;
			ret = tmp;
		}

		--n;
	}

	return ret;
}

void print_list(struct ListNode *l)
{
	while(l != NULL)
	{
		printf("%d ", l->val);
		l = l->next;
	}
	printf("\n");
}

int main(void)
{
	struct ListNode *l1 = NULL, *l2 = NULL, *l3 = NULL;
	int n1 = 0, n2 = 0;

	scanf("%d %d",&n1, &n2);
	l1 = create_list(n1);
	l2 = create_list(n2);
	l3 = addTwoNumbers(l1, l2);

	print_list(l1);
	print_list(l2);
	print_list(l3);

	free(l1);
	free(l2);
	free(l3);
	return 0;
}
