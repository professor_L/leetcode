#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 *  Link: https://leetcode.com/contest/biweekly-contest-60/problems/find-the-middle-index-in-array/
 */

int findMiddleIndex(int* nums, int numsSize)
{
	if (numsSize == 1)
		return 0;


	int leftsum = 0;
	int rightsum = 0;

	for (int i = 1; i < numsSize; ++i)
		rightsum += nums[i];

	int index = 0;
	do
	{
		if (rightsum == leftsum)
			return index;
		
		index += 1;
		if ( index < numsSize )
		{
			leftsum += nums[index-1];
			rightsum -= nums[index];
		}
		else
			break;

	}while(1);

	return -1;
}

int main(int argc, char *argv[])
{
	int n;
	scanf("%d", &n);

	int *a = (int*)malloc( n * sizeof(int) );
	for (int i = 0; i < n; ++i)
		scanf("%d", &a[i]);

	printf("%d\n", findMiddleIndex(a, n));
	free(a);
	return 0;
}
