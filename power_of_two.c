#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 *	Link: https://leetcode.com/problems/power-of-two/
 */

bool isPowerOfTwo(int n)
{
	if ( n == 0 )
		return false;

	while ( n % 2 == 0 )
		n = n / 2;

	return n == 1;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	if ( isPowerOfTwo(n) )
		printf("true");
	else
		printf("false");

	return 0;
}
