#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/remove-duplicates-from-sorted-list/
 */

struct ListNode* deleteDuplicates(struct ListNode* head)
{
    if (head)
    {
        struct ListNode *root = head, *last = head;
        int tmp = root->val;
        root = root->next;
        while(root)
        {
            if (tmp == root->val)
                last->next = root->next;
            else
            {
                tmp = root->val;
                last = root;
            }

            root = root->next;
        }
    }
	return head;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	return 0;
}
