#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/*
 * 	Link: https://leetcode.com/problems/construct-the-rectangle/
 */

int* constructRectangle(int area, int* returnSize)
{
    *returnSize = 2;
    int *ret = (int*)malloc( sizeof(int) * 2 );

    int l, w;
    double dw = sqrt(area);
    w = dw;

    if ( dw == floor(dw) )
        l = area / w;
    else
    {
        for ( ; w > 1; --w)
            if (area % w == 0)
                break;
    }

    l = area / w;

    ret[0] = l;
    ret[1] = w;
    return ret;
}


int main(int argc, char *argv[])
{
	int area = atoi(argv[1]);
	int size;
	int *ret = constructRectangle(area, &size);
	printf("%d %d\n", ret[0], ret[1]);
	free(ret);
	return 0;
}
