#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * 		Link: https://leetcode.com/problems/perfect-number/
 * 		Idea for algorithm: https://www.britannica.com/science/perfect-number
 */

bool isPrime(int n)
{
	if ( n == 2 || n == 3 )
		return true;

	int bound = n/2 + 1;
	for (int i = 3; i < bound; i+= 2)
		if ( n % i == 0 )
			return false;

	return true;
}

bool checkPerfectNumber(int num)
{
	if ( num & 0x01 )
		return false;

	int i = 1;
	int sum = 1;
	int product;

	do
	{
		i = i << 1;
		sum += i;

		product = sum * i;

	}while(product < num);

	return ( product == num && isPrime(sum) );
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);

	if ( checkPerfectNumber(n) )
		printf("true");
	else
		printf("false");

	return 0;
}
