#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/climbing-stairs/
 */


int combination(int n, int k)
{
	if (k == 0 || k == n )
		return 1;

	if ( k == 1 || k == n - 1 )
		return n;

	if ( n - k < k )
		k = n - k;

	int ret = (n - k + 1) * ( n - k + 2 ) / 2;
	for (int i = 3; i <= k; ++i)
		ret = ret*(n - k + i)/i;

	return ret;
}

int climbStairs(int n)
{
	if ( n < 4 )
		return n;

	int ret = n;
	n -= 2;
	for (int i = 2; i <= n; ++i, --n)
		ret += combination(n,i);

	return ret;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	printf("%d\n", climbStairs(n) );
	return 0;
}
