#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/*
 *  Link: https://leetcode.com/problems/plus-one/
 */

/*
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* plusOne(int* digits, int digitsSize, int* returnSize)
{
	*returnSize = digitsSize + 1;
	int *ret = (int*)calloc( *returnSize , sizeof(int) );

	int residual = 1;
	int i = digitsSize - 1;
	for (; i >= 0; --i)
	{
		ret[i+1] = (digits[i] + residual) % 10;
		residual = (digits[i] + residual) / 10;
	}

	ret[0] = residual;

	if (ret[0] == 1 )
		return ret;
	else
	{
		*returnSize -= 1;
		memmove(ret, ret+1, *returnSize * sizeof(int) );
		return ret;
	}
}


int main(int argc, char *argv[])
{
	int n = 0;
	scanf("%d",&n);

	int *a = (int*)malloc( n * sizeof(*a) );
	for (int i = 0; i < n; ++i)
		scanf("%d", &a[i]);

	int size = 0;
	int *ret = plusOne(a, n, &size);

	for (int i = 0; i < size; ++i)
		printf("%d ", ret[i]);

	free(a);
	free(ret);

	return 0;
}
