#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/find-greatest-common-divisor-of-array/
 */


static inline void swap(int *a, int *b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

static inline int gcd(int a, int b)
{
	if ( a == 0 || b == 0 )
		return 0;

	if ( a < b )
		swap(&a, &b);

	unsigned long long  r = b;
	while ( r != 0 )
	{
		r = a % b;

		a = b;
		b = r;

	}

	return a;
}

int findGCD(int* nums, int numsSize)
{
    int min = nums[0];
    int max = nums[0];
    
    for (int i = 1; i < numsSize; ++i)
    {
        if ( min > nums[i])
            min = nums[i];
        if ( max < nums[i])
            max = nums[i];
    }
    
    return gcd(max,min);
}


int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	return 0;
}
