#include <iostream>
#include <stack>
#include <string>

using namespace std;

/*
 * 	Link: https://leetcode.com/problems/valid-parentheses
 *	Data structure: stack
 *  Algorithm:
 *  	- use a loop to examine each character in a string
 *  	- if it is an open bracket '(', '[', '{' ---> push to stack
 *  	- if it is a closed bracket ')', ']', '}'
 *  		+ get an element from a stack and compare if it is a match
 *  	- loop until we read all the character in string
 *  
 *  Cases to consider:
 *  - open brackets are MORE than close brackets
 *  - open brackets are LESS than close brackets
 */

bool isValid(std::string str)
{
    std::stack<char> s;
	char c;
	char e;

	size_t size = str.size();


	for (size_t i = 0; i < size; ++i)
	{
		c = str[i];
		if ( c == '(' || c == '{' || c == '[' )
			s.push(c);
		else
		{
			if ( s.empty() )
				return false;

			e = s.top();
			s.pop();

			if ( e == '(' && c != ')' )
				return false;
			if ( e == '[' && c != ']' )
				return false;
			if ( e == '{' && c != '}' )
				return false;
		}
	}

	if ( s.size() != 0 )
		return false;
	return true;
}

int main(int argc, char *argv[])
{
	std::string s(argv[1]);
	
	if ( isValid(s) )
		std::cout << "true";
	else
		std::cout << "false";

	return 0;
}
