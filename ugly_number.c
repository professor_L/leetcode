#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

/*
 *	Link: https://leetcode.com/problems/ugly-number/
 */

bool isUgly(int n)
{
	if (n == 0)
		return false;

	while (n % 4 == 0)
		n = n / 4;

	while (n % 9 == 0)
		n = n / 9;

	while (n % 25 == 0)
		n = n / 25;

	while (n % 2 == 0)
		n = n / 2;

	while (n % 3 == 0)
		n = n / 3;

	while (n % 5 == 0)
		n = n / 5;

	return n == 1;
}


int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	if ( isUgly(n) )
		printf("true");
	else
		printf("false");

	return 0;
}
