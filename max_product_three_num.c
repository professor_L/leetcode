#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/maximum-product-of-three-numbers/
 */

static inline int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int maximumProduct(int* nums, int size)
{
    if ( size == 3)
        return nums[0] * nums[1] * nums[2];

	qsort(nums, size, sizeof(int), compare);
	int left = nums[0] * nums[1] * nums[size-1];
	int right = nums[size-1] * nums[size-2] * nums[size-3];

	if ( left > right )
		return left;
	else
		return right;

}

int main(int argc, char *argv[])
{
	int n;
	scanf("%d", &n);
	int *a = (int*)malloc( sizeof(int) * n );
	for (int i = 0; i < n; ++i )
		scanf("%d", &a[i]);

	printf("%d\n", maximumProduct(a, n));
	
	for (int i = 0; i < n; ++i )
		printf("%d ", a[i]);
	return 0;
}
