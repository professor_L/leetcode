#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

bool isPowerOfThree(int n)
{
	if ( n == 0 )
		return false;

	while ( n % 3 == 0 )
		n = n / 3;

	return n == 1;
	
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	if ( isPowerOfThree(n) )
		printf("true");
	else
		printf("false");

	return 0;
}
