#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/merge-two-sorted-lists/submissions/
 * 	Our task is to link nodes of two lists in correct order
 * 	Algorithm:
 * 		+ we will have a pointer tmp that points to current node
 * 		+ Run a loop, compare node from list A against node from list B
 * 		+ if ( A < B ), point tmp to A. Update tmp and A
 * 			# tmp->next = A
 * 			# tmp = A
 * 			# A = A->next
 * 		+ If ( B > A ), do the same
 *		+ After the loop, A or B will run of of numbers (NULL). 
 *		+ Point tmp to the non-NULL list.
 *
 */

struct ListNode* mergeTwoLists(struct ListNode* l, struct ListNode* r)
{
    if ( l == NULL && r == NULL)
        return NULL;
    
    if (l == NULL)
        return r;
    if (r == NULL)
        return l;
    
    struct ListNode* ret;
    if ( l->val < r->val )
    {
        ret = l;
        l = l->next;
    }
    else
    {
        ret = r;
        r = r->next;
    }
    
    struct ListNode *tmp = ret;
    while( l && r )
    {
        if ( l->val < r->val)
        {
            tmp->next = l;
            tmp = l;
            l = l->next;
        }
        else
        {
            tmp->next = r;
            tmp = r;
            r = r->next;
        }
    }
    
    if (l == NULL )
        tmp->next = r;
    if (r == NULL )
        tmp->next = l;
    
    return ret;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	return 0;
}
