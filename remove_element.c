#include <stdio.h>
#include <stdlib.h>

int removeElement(int* nums, int numsSize, int val)
{
	if ( numsSize == 1 && nums[0] == val)
        return 0;
    
	int ret = numsSize;
	int j = numsSize - 1;
	int i = 0;

	for (; i < j ; ++i)
	{
		if ( nums[i] == val )
		{
		    --ret;
			for ( ; j > i ; --j)
			{
				if ( nums[j] != val )
				{
					nums[i] = nums[j];
					j--;
					break;
				}
				else
					--ret;
			}
		}
	}

	if ( i == j && nums[i] == val )
		ret--;

	return ret;
}

int main(int argc, char *argv[])
{
	int n, val;
	scanf("%d %d",&n, &val);

	int *a = (int*)malloc( n * sizeof(*a) );
	for (int i = 0; i < n; ++i)
		scanf("%d", &a[i]);

	printf("k = %d\n", removeElement(a, n, val));
	for (int i = 0; i < n; ++i)
		printf("%d ", a[i]);
	free(a);
	return 0;
}
