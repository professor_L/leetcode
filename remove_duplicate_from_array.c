#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/remove-duplicates-from-sorted-array/
 */

int removeDuplicates(int* a, int n)
{
	if ( n < 2 )
        return n;
    
    int k = 0;
    int value;
    for (int i = 0; i < n; ++i)
    {
        value = a[k];
        while( i < n && a[i] <= value)
            ++i;
        
        if ( i == n )
            break;
        
        k++;
        a[k] = a[i];
    }
    
    return k+1;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	return 0;
}
