#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * 	Link: https://leetcode.com/problems/check-if-it-is-a-straight-line/
 */

static inline bool isBelong(int point[], int vector[], int tbc[])
{
	return ( vector[0]*(point[0] - tbc[0]) + vector[1]*(point[1] - tbc[1]) ) == 0;
}

bool checkStraightLine(int** coordinates, int coordinatesSize, int* coordinatesColSize)
{
	if (coordinatesSize == 2)
		return true;

	int vector[2] = {coordinates[0][1] - coordinates[1][1],
						coordinates[1][0] - coordinates[0][0] };

	for (int i = 2; i < coordinatesSize; ++i)
	{
		if ( isBelong(coordinates[0], vector, coordinates[i]) != true ) 
				return false;
	}

	return true;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	return 0;
}
