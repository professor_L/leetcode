#include <stdio.h>
#include <stdbool.h>

/*
 * Link: https://leetcode.com/problems/palindrome-number/
 */

bool isPalindrome(int x){
    if ( x < 0 )
        return false;

	if ( x < 10 )
		return true;

	int rev = 0;
	int tmp = x;
	while (tmp)
	{
		rev = rev * 10 + tmp % 10;
		tmp = tmp / 10;
	}

	return rev == x;
}

int main(void)
{
	int x;
	scanf("%d",&x);
	
	if ( isPalindrome(x) )
		printf("true");
	else
		printf("false");

	return 0;
}
