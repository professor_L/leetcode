#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int romanToInt(char * s)
{
	int len = strlen(s);
	int tmp = -1;
	int cur;
	int ret = 0;
	
	for (int i = len - 1; i > -1; --i)
	{
		switch (s[i])
		{
		case 'I':
			cur = 1;
			break;
		case 'V':
			cur = 5;
			break;
		case 'X':
			cur = 10;
			break;
		case 'L':
			cur = 50;
			break;
		case 'C':
			cur = 100;
			break;
		case 'D':
			cur = 500;
			break;
		case 'M':
			cur = 1000;
			break;
		}

		if ( cur < tmp )
			ret -= cur;
		else
			ret += cur;
		tmp = cur;
	}


	return ret;
}

int main(int argc, char *argv[])
{
	printf("%d", romanToInt(argv[1]));
}
