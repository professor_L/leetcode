#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 *  Link: https://leetcode.com/problems/happy-number/
 */

static inline int sum_square(int n)
{
	int ret = 0;
	int digit = 0;
	while(n)
	{
		digit = n % 10;
		ret += digit * digit;
		n = n / 10;
	}
	return ret;

}

int mycompare(const void *a, const void *b)
{
	return ( *(int*)a - *(int*)b );
}

bool isHappy(int n)
{
	int happy_list[20] = {1, 7, 10, 13, 19, 23, 28, 31, 32, 44,
						49, 68, 70, 79, 82, 86, 91, 94, 97, 100};

	while ( n > 100 )
		n = sum_square(n);

	int *search = bsearch(&n, happy_list, 20, sizeof(int), mycompare);
	return search != NULL;

}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	if ( isHappy(n) )
		printf("true");
	else
		printf("false");

	return 0;
}
