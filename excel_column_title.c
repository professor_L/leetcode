#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/*
 * 	Link: https://leetcode.com/problems/excel-sheet-column-title/
 */

char * convertToTitle(int num)
{
	int len = 0, r = 0;
	int m = num;
	while ( m )
	{
		len += 1;

		r = m % 26;
		if ( r == 0 )
			m -= 1;

		m = m /26;
	}
    
    char *ret = (char*)calloc( len + 1, 1); //plus one for null
    
    int i = len-1;
    
    while ( num )
    {
		r = num % 26 ;
		if ( r == 0 )
		{
			r = 26;
			num -= 1;
		}

        ret[i] = r + '@';
		num = num / 26;
		--i;
    }
    
    return ret;
}


int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	char *s = convertToTitle(n);

	printf("%s\n", s);
	free(s);
	return 0;
}
