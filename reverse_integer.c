#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define up_limit INT_MAX/10

int reverse(int x) 
{
	 if ( x == -2147483648 )
        return 0;
    
    int flag = 0;
    int ret = 0;
    
    if ( x < 0 )
    {
        x = -x;
        flag = 1;
    }
    
	while (x)
	{
		if ( ret > up_limit )
            return 0;
      
        
		ret = ret * 10 + (x%10);
		x = x / 10;
	}
	
	if (flag)
        ret = -ret;
    
	return ret;
}

int main(void)
{
	int x;
	scanf("%d",&x);
	printf("%d", reverse(x));
	return 0;
}
